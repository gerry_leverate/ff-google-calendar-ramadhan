<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    public function getEvents(){
        $events = Event::all();
        return $events;
    }
}
