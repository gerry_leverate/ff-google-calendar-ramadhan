<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use App\Exports\EventsExport;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        return view('admin.dashboard');
    }

    public function events(){
        $events = DB::select('select * from events');
        return view('admin.events',['events'=>$events]);
    }

    public function downloadEvents($type)
    {
        return Excel::download(new EventsExport, 'events.'.$type);
    }

    public function showFormAddEvent(){
        return view('admin.events.add');
    }

    public function report(){
        $events = DB::select('select * from events');
        return view('admin.report',['events'=>$events]);
    }
}
