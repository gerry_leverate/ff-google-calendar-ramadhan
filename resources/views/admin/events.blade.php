@extends('admin.layouts.layout')

@section('content')

<div class="box">
            <div class="box-header">
              <h3 class="box-title">.:: Event List ::.</h3>
              <div class="pull-right" style="padding-right:30px;">
              <a href="{{ route('admin.event.add') }}" class="btn btn-info"><i class="fa fa-plus-square"></i>&nbsp; Add Event</a>
                <div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-gear"></i>&nbsp; Options</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('admin/downloadEvents/xls') }}">Export to Excel</a></li>
                    <li><a href="#">Another action</a></li>
                  </ul>
                </div>
              </div>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="events" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Event Name</th>
                  <th>Date</th>
                  <th>Description</th>
                  <th>Action(s)</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($events as $event)
                <tr>
                  <td>{{ $event->event_name }}</td>
                  <td>{{ $event->start_event }}
                  </td>
                  <td>{{ $event->event_description }}</td>
                  <td>
                    <a href="#" class="btn btn-info">Detail</a>  
                    <a href="#" class="btn btn-primary">Update</a>  
                    <a href="#" class="btn btn-danger">Delete</a>  
                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Event Name</th>
                  <th>Date</th>
                  <th>Description</th>
                  <th>Action(s)</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

@endsection