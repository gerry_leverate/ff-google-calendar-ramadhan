@extends('admin.layouts.layout')

@section('content')
<!-- /.col (left) -->
<div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Add New Event</h3>
            </div>
            <div class="box-body">
                <!-- form start -->
            <form role="form">
              <div class="box-body col-md-6">
                <div class="form-group">
                  <label>Event Name</label>
                  <input type="event_name" class="form-control" placeholder="Enter event name">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                    <label>Start Date and End Date Time:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control pull-left" id="reservationtime">
                    </div>
                    <!-- /.input group -->
                </div>
              </div>

                <div class="box-body">
                    <div class="form-group">
                    <label>Event Description</label>
                        <div class="box-body pad">
                            <textarea class="textarea" placeholder="Place event description here"
                                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                    </div>
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                  <a href="#" class="btn btn-default"><i class="fa fa-mail-reply"></i> &nbsp;Back to event</a>
                <button type="submit" class="btn btn-primary">Save Event</button>
              </div>
            </form>



            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>

@endsection