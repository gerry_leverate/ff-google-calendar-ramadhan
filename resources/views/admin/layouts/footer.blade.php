<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; {{date('Y')}} <a href="https://leverate.co.id/">Leverate Media</a>.</strong> All rights
    reserved.
  </footer>