<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Frisian Flag -Calendar Events Management</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- UIkit CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
        <!-- UIkit JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>

    </head>
    <body>
    <div uk-sticky class="uk-sticky">
        <nav class="uk-navbar-container uk-margin" uk-navbar>
            <div class="uk-navbar-left">

                <a class="uk-navbar-item uk-logo" href="#target">Logo</a>

            </div>
        </nav>
    </div>
    

    <div class="uk-container uk-container-large uk-position-relative">
        <div class="uk-grid-small uk-child-width-expand@s uk-text-center uk-grid" uk-grid="">
            <div class="uk-first-column">
                <div class="uk-card "></div>
            </div>
            <div class="">
                <div class="uk-card uk-card-body uk-animation-slide-left">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </div>
            </div>
            <div class="">
                <div class="uk-card"></div>
            </div>
        </div>

        <div class="uk-grid-small uk-child-width-expand@s uk-text-center uk-grid" uk-grid>
            <div>
                <div class="uk-card"></div>
            </div>
            <div>
                <div class="uk-card">
                    <a class="uk-button uk-button-danger" href="#target" uk-scroll>How To</a>
                </div>
            </div>
            <div>
                <div class="uk-card ">
                <a href="<?php echo e(url('cal')); ?>" class="uk-button uk-button-primary">Start Events</a></div>
            </div>
            <div class="">
                <div class="uk-card "></div>
            </div>
            <div class="uk-flex-first">
                <div class="uk-card"></div>
            </div>
            <div>
                <div class="uk-card "></div>
            </div>
        </div>

        <div id="target" class="uk-padding uk-margin-large-top uk-margin-medium-bottom">
            <div class="uk-child-width-1-3@m uk-grid" uk-grid="">
                <div class="uk-first-column">
                    <div class="uk-card uk-card-default uk-scrollspy-inview uk-animation-slide-left">
                        <div class="uk-card-media-top">
                            <img src="https://via.placeholder.com/1800x1200/1e87f0/ffffff" alt="" >
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">Media Top</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-first-column">
                    <div class="uk-card uk-card-default uk-animation-slide-bottom-medium">
                        <div class="uk-card-media-top">
                            <img src="https://via.placeholder.com/1800x1200/1e87f0/ffffff" alt="">
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">Media Top</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-first-column">
                    <div class="uk-card uk-card-default uk-scrollspy-inview uk-animation-slide-right">
                        <div class="uk-card-media-top">
                            <img src="https://via.placeholder.com/1800x1200/1e87f0/ffffff" alt="">
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">Media Top</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
    <div class="uk-section-primary uk-section uk-section-small" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-slide-left-small; delay: false;">
        <div class="uk-container"><div class="uk-grid-margin uk-grid" uk-grid="">
            <div class="uk-grid-item-match uk-flex-middle uk-width-expand@m uk-first-column">
                <div class="uk-panel">            
                
                    <div class="uk-margin-remove-vertical uk-text-left@m uk-text-center uk-scrollspy-inview uk-animation-slide-left-small" uk-scrollspy-class="" style="">
                        <a class="el-link" href="#">
                            Logo
                        </a>
                    </div>
                </div>
            </div>

            <div class="uk-grid-item-match uk-flex-middle uk-width-1-2@m">
                <div class="uk-panel">            
                    <div class="uk-text-center uk-scrollspy-inview uk-animation-slide-bottom-small" uk-scrollspy-class="uk-animation-slide-bottom-small" style="">
                        <ul class="uk-subnav uk-margin-remove-bottom uk-subnav-divider uk-flex-center" uk-margin="">
                        <li class="el-item uk-first-column">
                            <a class="el-link" href=#">Contact</a>
                        </li>
                        <li class="el-item">
                            <a class="el-link" href="#">Legal information</a>
                        </li>
                        <li class="el-item">
                            <a class="el-link" href="#">Partners</a>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="uk-grid-item-match uk-flex-middle uk-width-expand@m">
                    <div class="uk-panel">    
                        <div class="uk-margin-remove-vertical uk-text-right@m uk-text-center uk-scrollspy-inview uk-animation-slide-right-small" uk-scrollspy-class="uk-animation-slide-right-small" style="">    <div class="uk-child-width-auto uk-grid-small uk-flex-right@m uk-flex-center uk-grid" uk-grid="">
                                <div class="uk-first-column">
                                    <a class="el-link uk-icon-button uk-icon" href="http://www.facebook.com/" uk-icon="icon: facebook;"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="facebook"><path d="M11,10h2.6l0.4-3H11V5.3c0-0.9,0.2-1.5,1.5-1.5H14V1.1c-0.3,0-1-0.1-2.1-0.1C9.6,1,8,2.4,8,5v2H5.5v3H8v8h3V10z"></path></svg></a>
                                </div>
                                <div>
                                    <a class="el-link uk-icon-button uk-icon" href="http://www.instagram.com" uk-icon="icon: instagram;"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="instagram"><path d="M13.55,1H6.46C3.45,1,1,3.44,1,6.44v7.12c0,3,2.45,5.44,5.46,5.44h7.08c3.02,0,5.46-2.44,5.46-5.44V6.44 C19.01,3.44,16.56,1,13.55,1z M17.5,14c0,1.93-1.57,3.5-3.5,3.5H6c-1.93,0-3.5-1.57-3.5-3.5V6c0-1.93,1.57-3.5,3.5-3.5h8 c1.93,0,3.5,1.57,3.5,3.5V14z"></path><circle cx="14.87" cy="5.26" r="1.09"></circle><path d="M10.03,5.45c-2.55,0-4.63,2.06-4.63,4.6c0,2.55,2.07,4.61,4.63,4.61c2.56,0,4.63-2.061,4.63-4.61 C14.65,7.51,12.58,5.45,10.03,5.45L10.03,5.45L10.03,5.45z M10.08,13c-1.66,0-3-1.34-3-2.99c0-1.65,1.34-2.99,3-2.99s3,1.34,3,2.99 C13.08,11.66,11.74,13,10.08,13L10.08,13L10.08,13z"></path></svg></a>
                                </div>
                                <div>
                                    <a class="el-link uk-icon-button uk-icon" href="http://www.twitter.com/" uk-icon="icon: twitter;"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="twitter"><path d="M19,4.74 C18.339,5.029 17.626,5.229 16.881,5.32 C17.644,4.86 18.227,4.139 18.503,3.28 C17.79,3.7 17.001,4.009 16.159,4.17 C15.485,3.45 14.526,3 13.464,3 C11.423,3 9.771,4.66 9.771,6.7 C9.771,6.99 9.804,7.269 9.868,7.539 C6.795,7.38 4.076,5.919 2.254,3.679 C1.936,4.219 1.754,4.86 1.754,5.539 C1.754,6.82 2.405,7.95 3.397,8.61 C2.79,8.589 2.22,8.429 1.723,8.149 L1.723,8.189 C1.723,9.978 2.997,11.478 4.686,11.82 C4.376,11.899 4.049,11.939 3.713,11.939 C3.475,11.939 3.245,11.919 3.018,11.88 C3.49,13.349 4.852,14.419 6.469,14.449 C5.205,15.429 3.612,16.019 1.882,16.019 C1.583,16.019 1.29,16.009 1,15.969 C2.635,17.019 4.576,17.629 6.662,17.629 C13.454,17.629 17.17,12 17.17,7.129 C17.17,6.969 17.166,6.809 17.157,6.649 C17.879,6.129 18.504,5.478 19,4.74"></path></svg></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
        
    
</div>
    
        

        

    </body>
</html>
<?php /**PATH C:\xampp\htdocs\ff-google-calendar-ramadhan\resources\views/welcome.blade.php ENDPATH**/ ?>